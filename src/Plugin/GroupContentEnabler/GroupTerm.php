<?php

namespace Drupal\group_term\Plugin\GroupContentEnabler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Plugin\GroupContentEnablerBase;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Provides a content enabler for taxonomy terms.
 *
 * @GroupContentEnabler(
 *   id = "group_term",
 *   label = @Translation("Group term"),
 *   description = @Translation("Adds taxonomy terms to a group."),
 *   entity_type_id = "taxonomy_term",
 *   entity_access = TRUE,
 *   deriver = "Drupal\group_term\Plugin\GroupContentEnabler\GroupTermDeriver",
 *   handlers = {
 *     "access" = "Drupal\group\Plugin\GroupContentAccessControlHandler",
 *     "permission_provider" = "Drupal\group\Plugin\GroupContentPermissionProvider",
 *   }
 * )
 */
class GroupTerm extends GroupContentEnablerBase {

  /**
   * Retrieves the vocabulary this plugin supports.
   *
   * @return \Drupal\taxonomy\VocabularyInterface|\Drupal\Core\Entity\EntityInterface
   *   The Vocabulary this plugin supports.
   */
  protected function getVocabulary() {
    return Vocabulary::load($this->getEntityBundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupOperations(GroupInterface $group) {
    $account = \Drupal::currentUser();
    $plugin_id = $this->getPluginId();
    $vocabulary = $this->getEntityBundle();
    $operations = [];

    if ($group->hasPermission("create $plugin_id entity", $account) || $account->hasPermission('create any group_term entity')) {
      $route_params = ['group' => $group->id(), 'plugin_id' => $plugin_id];
      $operations["group_term-create-$vocabulary"] = [
        'title' => $this->t('Create @type', ['@type' => $this->getVocabulary()->label()]),
        'url' => new Url('entity.group_content.create_form', $route_params),
        'weight' => 30,
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['entity_cardinality'] = 1;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $dependencies['config'][] = 'taxonomy.vocabulary.' . $this->getEntityBundle();
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function createEntityAccess(GroupInterface $group, AccountInterface $account) {
    if ($account->hasPermission('create any group_term entity')) {
      return AccessResult::allowed()->addCacheContexts(['user.permissions']);
    }

    return parent::createEntityAccess($group, $account)->addCacheContexts(['user.permissions']);
  }

}
