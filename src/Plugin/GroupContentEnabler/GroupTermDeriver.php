<?php

namespace Drupal\group_term\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Provides derivative definitions for vocabularies.
 */
class GroupTermDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (Vocabulary::loadMultiple() as $name => $vocabulary) {
      $label = $vocabulary->label();

      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => t('Group term (@label)', ['@label' => $label]),
        'description' => t('Adds %type terms to groups.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
