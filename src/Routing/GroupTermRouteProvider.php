<?php

namespace Drupal\group_term\Routing;

use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for group_node group content.
 */
class GroupTermRouteProvider {

  /**
   * Provides the shared collection route for group node plugins.
   */
  public function getRoutes() {
    $routes = $plugin_ids = $permissions_add = $permissions_create = [];

    foreach (Vocabulary::loadMultiple() as $name => $vocabulary_type) {
      $plugin_id = "group_term:$name";

      $plugin_ids[] = $plugin_id;
      $permissions_add[] = "create $plugin_id content";
      $permissions_create[] = "create $plugin_id entity";
    }

    // If there are no vocabularies yet, we cannot have any plugin IDs and
    // should therefore exit early because we cannot have any routes for them
    // either.
    if (empty($plugin_ids)) {
      return $routes;
    }

    $routes['entity.group_content.group_term_relate_page'] = new Route('group/{group}/term/add');
    $routes['entity.group_content.group_term_relate_page']
      ->setDefaults([
        '_title' => 'Add an existing term',
        '_controller' => '\Drupal\group_term\Controller\GroupTermController::addPage',
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_add))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    $routes['entity.group_content.group_term_add_page'] = new Route('group/{group}/term/create');
    $routes['entity.group_content.group_term_add_page']
      ->setDefaults([
        '_title' => 'Add a new term',
        '_controller' => '\Drupal\group_term\Controller\GroupTermController::addPage',
        'create_mode' => TRUE,
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_create))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    return $routes;
  }

}
