<?php

namespace Drupal\Tests\group_term\Functional;

use Drupal\Tests\group\Functional\EntityOperationsTest as GroupEntityOperationsTest;

/**
 * Tests that entity operations (do not) show up on the group overview.
 *
 * @see group_term_entity_operation()
 *
 * @group group_term
 */
class EntityOperationsTest extends GroupEntityOperationsTest {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['group_term'];

  /**
   * {@inheritdoc}
   */
  public function provideEntityOperationScenarios() {
    $scenarios['withoutAccess'] = [
      [],
      ['group/1/terms' => 'Terms'],
    ];

    $scenarios['withAccess'] = [
      [],
      ['group/1/terms' => 'Terms'],
      ['access group_term overview'],
    ];

    $scenarios['withAccessAndViews'] = [
      ['group/1/terms' => 'Terms'],
      [],
      ['access group_term overview'],
      ['views'],
    ];

    return $scenarios;
  }

}
