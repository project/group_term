<?php

namespace Drupal\Tests\group_term\Functional;

use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\group\Functional\GroupBrowserTestBase;

/**
 * Tests that we can see local action links.
 *
 * @group group_term
 */
class GroupTermsTest extends GroupBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'path',
    'views',
    'taxonomy',
    'group',
    'group_test_config',
    'group_term',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'bartik';

  /**
   * Vocabulary id.
   *
   * @var string
   */
  protected $vid;

  /**
   * User.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * Group.
   *
   * @var \Drupal\group\Entity\Group
   */
  protected $group;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->group = $this->createGroup();

    $this->vid = $this->randomMachineName();
    $plugin_id = "group_term:{$this->vid}";
    $vocabulary = Vocabulary::create([
      'name' => $this->randomMachineName(),
      'vid' => $this->vid,
    ]);
    $vocabulary->save();

    // Install group_membership_request group content.
    $storage = $this->entityTypeManager->getStorage('group_content_type');
    $config = [
      'group_cardinality' => 0,
      'entity_cardinality' => 1,
    ];

    $storage->createFromPlugin($this->group->getGroupType(), $plugin_id, $config)->save();

    \Drupal::service('router.builder')->rebuild();

    // Allow see and create terms.
    $role = $this->group->getGroupType()->getOutsiderRole();
    $role->grantPermissions([
      'access group_term overview',
      "create $plugin_id content",
      "create $plugin_id entity",
    ]);
    $role->save();

    $this->drupalPlaceBlock('local_actions_block');

    $this->user = $this->createUser();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests local action links.
   */
  public function testLocalActionLinks() {
    $this->drupalGet("/group/{$this->group->id()}/terms");
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->linkExists('Add term');
    $this->assertSession()->linkExists('Add existing term');
  }

  /**
   * Test term creation inside of group.
   */
  public function testTermCreation() {
    $this->drupalGet("/group/{$this->group->id()}/term/create");
    $this->assertSession()->statusCodeEquals(200);

    $submit_button = 'Save';
    $this->assertSession()->buttonExists($submit_button);

    $name = $this->randomString();
    $edit = [
      'name[0][value]' => $name,
    ];

    $this->submitForm($edit, $submit_button);
    $this->assertSession()->pageTextContains('Created new term');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test term relation inside of group.
   */
  public function testTermRelation() {
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->create([
      'name' => $this->randomString(),
      'vid' => $this->vid,
    ]);
    $term->save();

    $this->drupalGet("/group/{$this->group->id()}/term/add");
    $this->assertSession()->statusCodeEquals(200);

    $submit_button = 'Save';
    $this->assertSession()->buttonExists($submit_button);

    $edit = [
      'entity_id[0][target_id]' => $term->id(),
    ];

    $this->submitForm($edit, $submit_button);
    $this->assertSession()->statusCodeEquals(200);
  }

}
